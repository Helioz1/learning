spam = ['cow', 'pig', 'bull', 'rabbit', 'lion']
spam.remove('cow')
spam.append('chicken')
spam.sort()

try:
    print(spam)

except ValueError:
    print("spam must be in quotes")

egg = 'chicken'
try:
    egg.remove('chicken')
except AttributeError:
    print('you cannot directly remove from a variable since no attributes can intuitevely work with variables')