# All projects with CD stands for CodeAcademy
# The following are snippets from CodeAcademy
# Credit goes to CodeAcademy

def grade_converter(gpa):
  if gpa >= 4.0:
    return "A"
  elif gpa > 3.0:
    return "B"
  elif gpa > 2.0:
    return "C"
  elif gpa > 1.0:
    return "D"
  elif gpa > 0.0:
    return "F"

  
grade = grade_converter(5.0)

print(grade)