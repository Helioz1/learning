def applicant_selector(gpa: int = 0.2, ps_score: int = 97, ec_count: int = 4):
  if gpa >= 3.0 and ps_score >= 90 and ec_count >= 3:
    return "This applicant should be accepted."
  elif gpa >= 3.0 and ps_score >= 90:
    not ec_count < 3
    return "This applicant should be given an in-person interview."
  elif gpa <= 3.0 or ps_score <= 90 or ec_count <= 3:
    return "This applicant should be rejected."


print(applicant_selector())
