# All projects with CD stands for CodeAcademy
# The following are snippets from CodeAcademy
# Credit goes to CodeAcademy

def greater_than(x, y):
  if x > y:
    return x
  if y < x:
    return y
  if x == y:
    return "These numbers are the same"

  
def graduation_reqs(credits):
  if int(credits) >= 120:
    return "You have enough credits to graduate!"
  
  
print(graduation_reqs(120))