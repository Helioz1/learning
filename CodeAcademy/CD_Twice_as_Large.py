# All projects with CD stands for CodeAcademy
# The following are snippets from CodeAcademy
# Credit goes to CodeAcademy

# Write your twice_as_large function here:

def twice_as_large(num1, num2):
  if num1 > num2 * 2:
    return True
  else:
    return False

# Uncomment these function calls to test your twice_as_large function:
print(twice_as_large(0, 5))
# should print False
print(twice_as_large(11, 5))
# should print True

print(twice_as_large(20, 10))