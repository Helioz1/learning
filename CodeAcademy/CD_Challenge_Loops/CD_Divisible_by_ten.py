# Learn
# CODE CHALLENGE: LOOPS
# Divisible by Ten
# divisible_by_ten(nums)

# Instructions
# 1.
# Create a function named divisible_by_ten() that takes 
# a list of numbers named nums as a parameter.

# Return the amount of numbers in that list that are 
# divisible by 10.

#Write your function here
def divisible_by_ten(nums):
  # create the variable count
  count = 0
  # create the loop
  for number in nums:
  # the boolean statement can place a tuple to make it so that data 	 		structures cannot be modified
    if number % 10 == 0:
  # use the += shorthand operator to add to the count
      count += 1
  return count

#Uncomment the line below when your function is done
print(divisible_by_ten([20, 25, 30, 35, 40]))