#Write your function here

def add_greetings(names):
  name = []
  for x in names:
    name.append("Hello, " + x )
# don't forget to add the return at the end
# the return must refer to either a list or variable
# that is being modified
  return name

#Uncomment the line below when your function is done
print(add_greetings(["Owen", "Max", "Sophie"]))