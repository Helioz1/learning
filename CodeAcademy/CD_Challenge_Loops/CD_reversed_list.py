# def reversed_list(lst1, lst2):
#   for i in range(len(lst1)):
#     if lst1[i] == lst2[(-i - 1)]:
#       return True
#     else:
#       return False
#   return

# def reversed_list(lst1, lst2):
#   lst2_new=[]
#   while len(lst1)>len(lst2_new):
#     lst2_new.append(lst2[-1])
#     lst2=lst2[0:-1]
#   print(lst2_new)
    

# #Uncomment the lines below when your function is done
# print(reversed_list([1, 2, 3], [3, 2, 1]))
# print(reversed_list([1, 5, 3], [3, 2, 1]))

#Write your function here
def reversed_list(lst1, lst2):
  matches = []
  for index1 in range(len(lst1)):
    for index2 in range(len(lst2)):
      if lst1[index1] == lst2[index2]:
        matches.append(True)
  if matches.count(True) == len(lst1):
    return True
  else:
    return False
    

#Uncomment the lines below when your function is done
print(reversed_list([1, 2, 3], [3, 2, 1]))
print(reversed_list([1, 5, 3], [3, 2, 1]))

