# #Write your function here

# def larger_sum(lst1, lst2):
#   sum1 = 0
#   sum2 = 0
#   for index in lst1:
#     index + sum1
#     return sum1 == lst1
#   for index in lst2:
#     index + sum2
#     return sum2 == lst2
  
  

# #Uncomment the line below when your function is done
# print(larger_sum([1, 9, 5], [2, 3, 7]))

#Write your function here

def larger_sum(lst1, lst2):
    sum1  = 0 #create new variable for sum of numbers from list 1
    sum2  = 0 #create new variable for sum of numbers from list 2
    for number in lst1:
        sum1 += number
    for number in lst2:
        sum2 += number
    if sum1 >= sum2:
        return lst1
    else:
        return lst2
  

#Uncomment the line below when your function is done
print(larger_sum([1, 9, 5], [2, 3, 7]))