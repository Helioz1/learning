#Write your function here

# def exponents(bases, power):
#   new_lst = []
#   for x in bases:
#     x ** 2                                         
#     new_lst.append(x)
#     for y in power:
#       y ** x ** 2 
#       new_lst.append(y)
#   return new_lst
    

#Uncomment the line below when your function is done
# print(exponents([2, 3, 4], [1, 2, 3]))

def exponents(bases, powers):
    new_lst = []
    for base in bases:
        for power in powers:
            new_lst.append(base ** power)
    return new_lst

print(exponents([2, 3, 4], [1, 2, 3]))