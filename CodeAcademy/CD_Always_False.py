# Write your always_false function here:

def always_false(num):
  if 0 > num:
    return False
  elif 0 < num:
    return False
  elif 0 == num:
    return False

# Uncomment these function calls to test your always_false function:
print(always_false(0))
# should print False
print(always_false(-1))
# should print False
print(always_false(1))
# should print False