# All projects with CD stands for CodeAcademy
# The following are snippets from CodeAcademy
# Credit goes to CodeAcademy

statement_one = False

statement_two = True

def graduation_reqs(gpa: int = 0.0, credits: credits = 0):
  if (gpa >= 2.0) and (credits >= 120):
    return "You meet the requirements to graduate!"
  if (gpa >= 2.0):
    not (credits >= 120)
    return "You do not have enough credits to graduate."
  if (credits >= 120):
    not (gpa <= 2.0)
    return "Your GPA is not high enough to graduate."
  if (gpa != 2.0) and (credits != 120):
    return "You do not meet either requirement to graduate!"

print(graduation_reqs(0.0, 100))