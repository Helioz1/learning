# CODE CHALLENGE: LISTS
# Combine Sort
# combine_sort(lst1, lst2)

# Instructions
# 1.
# Write a function named combine_sort that has two 
# parameters named lst1 and lst2.

# The function should combine these two lists into 
# one new list and sort the result. 
# Return the new sorted list.

# First combine lst1 and lst2 using + and then 
# sort the list using .sort() or the sorted function.


#Write your function here

def combine_sort(lst1, lst2):
  lst = sorted(lst1 + lst2)
  return lst

#Uncomment the line below when your function is done
print(combine_sort([4, 10, 2, 5], [-10, 2, 5, 10]))