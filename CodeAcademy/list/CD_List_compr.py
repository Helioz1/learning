# LEARN PYTHON: LOOPS
# List Comprehensions
# Let’s say we have scraped a certain website and gotten these words:

# words = ["@coolguy35", "#nofilter", "@kewldawg54", "reply", "timestamp", "@matchamom", "follow", "#updog"]
# We want to make a new list, called usernames, that has all of the strings in words with an '@' as the first character. We know we can do this with a for loop:

# words = ["@coolguy35", "#nofilter", "@kewldawg54", "reply", "timestamp", "@matchamom", "follow", "#updog"]
# usernames = []

# for word in words:
#   if word[0] == '@':
#     usernames.append(word)
# First, we created a new empty list, usernames, and as we looped through the words list, we added every word that matched our criterion. Now, the usernames list looks like this:

# >>> print(usernames)
# ["@coolguy35", "@kewldawg54", "@matchamom"]
# Python has a convenient shorthand to create lists like this with one line:

# usernames = [word for word in words if word[0] == '@']
# This is called a list comprehension. It will produce the same output as the for loop did:

# ["@coolguy35", "@kewldawg54", "@matchamom"]
# This list comprehension:

# Takes an element in words
# Assigns that element to a variable called word
# Checks if word[0] == '@', and if so, it adds word to the new list, usernames. If not, nothing happens.
# Repeats steps 1-3 for all of the strings in words
# Note: if we hadn’t done any checking (let’s say we had omitted if word[0] == '@'), the new list would be just a copy of words:

# usernames = [word for word in words]
# usernames is now ["@coolguy35", "#nofilter", "@kewldawg54", "reply", "timestamp", "@matchamom", "follow", "#updog"]


heights = [161, 164, 156, 144, 158, 170, 163, 163, 157]

can_ride_coaster = []

for h in heights:
  if h > 161:
    can_ride_coaster.append(h)
    
print(can_ride_coaster)