# Instructions
# 1.
# Create a list called single_digits that consists of 
# the numbers 0-9 (inclusive).

# 2.
# Create a for loop that goes through single_digits 
# and prints out each one.

# 3.
# Create a list called squares. Assign it to be an 
# empty list to begin with.

# 4.
# Inside the loop that iterates through single_digits, 
# append the squared value of each element of single_digits 
# to the list squares. You can do this before or 
# after printing the element.

# 5.
# After the for loop, print out squares.

# 6.
# Create the list cubes using a list comprehension on the 
# single_digits list. Each element of cubes should be an 
# element of single_digits taken to the third power.

# 7.
# Print cubes.

# Good job!

single_digits = range(0, 10)
squares = []

print(squares)

for _ in single_digits:
  print(_)

#for x in single_digits:
	#x*x
  #squares.append(x)
  #print(squares)
  
#squares = [ x ** 2 for x in single_digits]
#print(squares)

for x in single_digits:
  squares.append(x ** 2)
  print(squares)

#cubes = []

cubes = [ x ** 3 for x in single_digits]

print(cubes)


