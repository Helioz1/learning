# All projects with CD stands for CodeAcademy
# The following are snippets from CodeAcademy
# Credit goes to CodeAcademy

names = ['Jenny', 'Alexus', 'Sam', 'Grace']
dogs_names = ['Elphonse', 'Dr. Doggy DDS', 'Carter', 'Ralph']

names_and_dogs_names = zip(names, dogs_names)

list_of_names_and_dogs_name = list(names_and_dogs_names)

print(names_and_dogs_names)
print(list_of_names_and_dogs_name)