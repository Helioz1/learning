# All projects with CD stands for CodeAcademy
# The following are snippets from CodeAcademy
# Credit goes to CodeAcademy


def cost_of_ground_shipping(weight):
    if weight <= 2:
      price_per_pound = 1.50
    elif weight <= 6:
      price_per_pound = 3.00
    elif weight <= 10:
      price_per_pound = 4.00
    else:
      price_per_pound = 4.75

    return 20 + (price_per_pound * weight)


print(cost_of_ground_shipping(8.4))


cost_of_premium_shipping = 125

print(cost_of_premium_shipping)

def cost_of_drone_shipping(weight):
    if weight <= 2:
      price_per_pound = 4.50
    elif weight <= 6:
      price_per_pound = 9.00
    elif weight <= 10:
      price_per_pound = 12.00
    else:
      price_per_pound = 14.25

    return (price_per_pound * weight)


print(cost_of_drone_shipping(1.5))

def print_best_price_for_shipping(weight):
  
  ground = cost_of_ground_shipping(weight)
  drone = cost_of_drone_shipping(weight)
  premium = cost_of_premium_shipping

  if ground < premium and ground < drone:
      method = "standard ground"
      cost = ground

  elif premium < ground and premium < drone:
      method = "premium"
      cost = premium

  else:
      method = "drone"
      cost = drone

  print("""The cheapest option available is $%.2f 
  with %s shipping"""
      % (cost, method)  
  )

print_best_price_for_shipping(4.8)
print_best_price_for_shipping(41.5)