# WORKING WITH LISTS IN PYTHON
# Review
# In this lesson, we learned how to:

# Get the length of a list
# Select subsets of a list (called slicing)
# Count the number of times that an element appears in a list
# Sort a list of items
# Instructions
# 1.
# inventory is a list of items that are in the warehouse for Bob’s Furniture. How many items are in the warehouse?

# Save your answer to inventory_len.

# 2.
# Select the first element in inventory. Save it to the variable first.

# 3.
# Select the last item from inventory and save it to the variable last.

# 4.
# Select items from the inventory starting at index 2 and up to, but not including, index 6.

# Save your answer to inventory_2_6.

# 5.
# Select the first 3 items of inventory and save it to the variable first_3.

# 6.
# How many 'twin bed's are in inventory? Save your answer to twin_beds.

# 7.
# Sort inventory using .sort().

inventory = ['twin bed', 'twin bed', 'headboard', 'queen bed', 'king bed', 'dresser', 'dresser', 'table', 'table', 'nightstand', 'nightstand', 'king bed', 'king bed', 'twin bed', 'twin bed', 'sheets', 'sheets', 'pillow', 'pillow']

inventory_len = len(list(inventory))

print(inventory_len)

first = inventory[0]

print(first)

last = inventory[18]

print(last)

inventory_2_6 = inventory[2:6]

print(inventory_2_6)

first_3 = inventory[0:3]

print(first_3)

twin_beds = inventory.count('twin bed')

print(twin_beds)

inventory.sort()

print(inventory)