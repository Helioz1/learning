#!/usr/bin/env python3

import discord #after import is the module that allows for configuration of the API
import configparser
from discord.ext import commands
import json
import os

client = discord.Client()

tokenparser = configparser.RawConfigParser()
tokenparser.read("token.ini")
token = tokenparser.get("Discord", "Token")

@client.event #event decorator/wrapper
async def on_ready(): #bot logs in itself
    print("We have logged in as {0.user}".format(client))

@client.event
async def on_member_join(member):
    with open('users.json', 'r') as f: # r for read, f for file
        users = json.load(f)
    
    await update_data(users, member)# here is where information will be manipulated, await function must be added to allow the data to be manipulated
    
    with open('users.json', 'w') as f: # w for write, f here is a temporary character
        json.dump(users, f)

@client.event
async def on_message(message):
    print(f"{message.channel} {message.author}") #the following only allows the bot to response to a user versus just a bot that could go infinitely
    if message.author == client.user:
        return
    with open('users.json', 'r') as f:
        users = json.load(f)

    await update_data(users, message.author)
    await add_experience(users, message.author, 5), 'add 5 exp'
    await level_up(users, message.author, message.channel)


    with open('users.json', 'w') as f:
        json.dump(users, f)

async def update_data(users, user):
    if not user.id in users:
        users[user.id] = {}
        users[user.id]['experience'] = 0
        users[user.id]['level'] = 1

async def add_experience(users, user, exp):
    users[user.id]['experience'] += exp

async def level_up(users, user, channel):
    experience = users[user.id]['experience']
    lvl_start = users[user.id]['level']
    lvl_end = int(experience ** (1/4))

    if lvl_start < lvl_end:
        await client.send_message(channel, '{} has level up to level {}' /format(user.mention, lvl_end))
        users[user.id]['level'] = lvl_end
    

client.run(token)