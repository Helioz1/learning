#!/usr/bin/env python3

import discord
import configparser

client = discord.Client()

tokenparser = configparser.RawConfigParser()
tokenparser.read("token.ini")
Kick = tokenparser.get("Discord", "Kick")

game = discord.Activity(type = discord.ActivityType.playing, name = "OH GOD PLS NO")

async def on_ready(): #bot logs itself in
    print("I am here to be kicked {0.user}".format(client)) #message on terminal when bot logs in
    await client.change_presence(activity = game)

@client.event
async def on_message(message):
    print(f"{message.channel} {message.author}")
    if message.author == client.user:
        return
    
    if message.content.startswith("hi"):
        await message.channel.send("I wish someone would say hi to me... But I am just waiting to be kicked...")
        return

    if "kick" in message.content.startswith("kick"):
        await message.channel.send("Yep")

client.run(Kick)