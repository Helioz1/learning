"""
Rest Client for Virustotal
"""


import sys
import requests
from requests.auth import HTTPBasicAuth

"""
For making rest call
"""

# if you do not initialize properly then you will get a
# error

class VTRest:

    def __init__(self, url=None, api=None,
    api_token=None, querystring=None):

        self.url = url
        self.api = api
        self.api_token = api_token
        self.querystring = querystring

    def rest_call(self, url=None, querystring=None):
