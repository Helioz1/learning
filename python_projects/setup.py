#!/usr/bin/env python

from distutils.core import setup

setup(name='Virus Tools',
      version='0.0.1',
      description='Python Distribution Utilities',
      author='Chris V',
      author_email='example',
      url='example',
      packages=['requests', 'bs4'],
     )