import random
import time
from pprint import pprint


class hero:
    def __init__(self, Hhealth, Hattack, Hluck, Hranged, Hdefence, Hmagic, Hname):
        self.health = Hhealth
        self.attack = Hattack
        self.luck = Hluck
        self.ranged = Hranged
        self.defence = Hdefence
        self.magic = Hmagic
        self.name = Hname

        def getHealth(self):
            return self.health  
        def getAttack(self):
            return self.attack
        def getLuck(self):
            return self.luck  
        def getRanged(self):
            return self.ranged
        def getDefence(self):
            return self.defence
        def getMagic(self):
            return self.magic
        def getName(self):
            return self.name    

        def setHealth(self, newHealth):
            set.health = newHealth
        def setattack(self, newAttack):
            set.attack = newAttack
        def setHealth(self, newLuck):
            set.luck = newLuck
        def setattack(self, newRanged):
            set.ranged = newRanged
        def setHealth(self, newdefence):
            set.defence = newdefence
        def setattack(self, newMagic):
            set.magic = newMagic
        def setHealth(self, newName):
            set.name = newName


def createClass():
    a = input("Are you more strategic(1) or more of a warrior(2)?...")
    while a != "1" and a != "2":
        print("invalid selection")
        a = input("Are you more strategic(1) or more of a warrior(2)?...")

    if a == "1":
        heroAttack = 50
        heroDefence = 100

    elif a == "2":
        heroAttack = 100
        heroDefence = 50

    b = input("Press enter to roll a dice...")
    time.sleep(0.2)
    print("rolling dice...")
    heroLuck = random.randint(0,10)
    print("you hero has", heroLuck, "luck out of 10")

    c = input("are you more of a bow and arrow user(1) or a magic user(2)?")
    while c != "1" and c!= "2":
        print ("invalid selection")
        c = input("are you more of a bow and arrow user(1) or a magic user(2)?")

    if c == "1":
        heroRanged = 100
        heroMagic = 50

    elif c == "2":
        heroRanged = 50
        heroMagic = 100

    heroName = input("What is your name hero??")
    print("welcome", heroName, "!!!")

    return(heroAttack, heroLuck, heroRanged, heroDefence, heroMagic, heroName)

class_data = createClass()

character = hero(100, class_data[0], class_data[1], class_data[2], class_data[3], class_data[4], class_data[5])
pprint(vars(character))