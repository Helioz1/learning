spam = [0, 1, 2, 3, 4, 5]
spam[2] = 'hello'
print(spam, " this is the original spam")
cheese = spam
cheese[3] = 'mod'
print(cheese, "this is the cheese list with a mod")
print(spam, "this is the spam list that has a mod due to the earlier cheese reference")